{config, lib, pkgs, ...}:
    let

    	risk_sim_build = pkgs.callPackage ./default.nix {};
    	cfg = config.services.risk_sim;


in {
    options.services.risk_sim.enable = lib.mkEnableOption "risk_sim";
    options.services.risk_sim.port = lib.mkOption {
        type = lib.types.int;
        default = 4000;
    };


    config = lib.mkIf cfg.enable {

    networking.firewall.allowedTCPPorts = [ cfg.port ];


    systemd.services.risk_sim = {
        description = "My home page";
        after = ["admin-pwd-key.service" "network-online.target" ];
    	wantedBy = ["admin-pwd-key.service" "network-online.target" ];



    	script = ''export HOME=/root
    		   export PORT=${toString cfg.port}
    		   export ADMIN_PWD=$(cat /run/keys/admin-pwd)
    		   export RELEASE_TMP=/tmp
    		   export Restart=always
    		   ${risk_sim_build}/bin/risk_sim start
    		   '';

     };
};
}


