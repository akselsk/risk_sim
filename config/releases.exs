 
import Config


secret_key_base =
  System.get_env("SECRET_KEY_BASE") || :crypto.strong_rand_bytes(64) |> Base.encode64 |> binary_part(0, 64)

application_port = System.fetch_env!("PORT")

config :risk_sim, RiskSimWeb.Endpoint,
  http: [:inet6, port: String.to_integer(application_port)],
  secret_key_base: secret_key_base
  #http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
config :risk_sim, RiskSimWeb.Endpoint, server: true
    #
# Then you can assemble a release by calling `mix release`.

 
