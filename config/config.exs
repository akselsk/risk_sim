# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :risk_sim, RiskSimWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4aW2mqdPAUG8njVl64rhcJrjsFGy6+ijhvda7UMtNavkxIMh1B6AhGdTS+4YG0th",
  render_errors: [view: RiskSimWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: RiskSim.PubSub,
  live_view: [signing_salt: "tT3TTcd+"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
