defmodule RiskSimWeb.PageLive do
  use RiskSimWeb, :live_view
  @dice [1,2,3,4,5,6]
  @attacker_throws [1,2,3]
  @defender_throws [1,2]

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, dead_attackers: 0, dead_defenders: 0, attacker_dice: [], defender_dice: [])}
  end

  @impl true
  def handle_event("fight", _, socket) do


      defender_results = Enum.map(@defender_throws, fn _x -> Enum.random(@dice) end) |> Enum.sort |> Enum.reverse
      attacker_result_tot = Enum.map(@attacker_throws, fn _x -> Enum.random(@dice) end)
      |>  Enum.sort |> Enum.reverse

      sum_result = Enum.map([0,1], fn x -> Enum.at(defender_results, x) - Enum.at(attacker_result_tot, x) end )

      dead_attackers = Enum.filter(sum_result, fn x -> x >= 0 end ) |> length

      {:noreply, assign(socket, attacker_dice: attacker_result_tot, defender_dice: defender_results, dead_attackers: dead_attackers, dead_defenders: 2- dead_attackers)}

  end
    

end
