

with import <nixpkgs> {};

let

risk_sim = {}:
 stdenv.mkDerivation rec {
  name = "risk_sim";
  src = ./.;
  buildInputs = [elixir git nodejs];

  #Certs are only needed if you have repos directlyu from github

  buildPhase = ''
  mkdir -p $PWD/.hex
  export HOME=$PWD/.hex
  export GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt
  export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
  mix local.rebar --force
  mix local.hex --force
  mix deps.get
  MIX_ENV=prod mix compile
  npm install --prefix ./assets
  npm run deploy --prefix ./assets
  mix phx.digest
  MIX_ENV=prod mix release

  '';
  installPhase = ''
    mkdir -p $out
    cp -rf _build/prod/rel/risk_sim/* $out/

  '';
};
in
risk_sim
